package com.example.calllog;

import android.Manifest;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import java.util.concurrent.TimeUnit;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity {

    private final int RC_READ_CALL_LOG = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startLog();
    }

    @AfterPermissionGranted(RC_READ_CALL_LOG)
    private void startLog() {
        String[] perms = {Manifest.permission.READ_CALL_LOG};
        if (EasyPermissions.hasPermissions(this, perms)) {
            PeriodicWorkRequest periodicWorkRequest =
                    new PeriodicWorkRequest.Builder(UploadWorker.class, 1, TimeUnit.MINUTES).build();
            WorkManager.getInstance(this).enqueue(periodicWorkRequest);
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, getString(R.string.calllog_perm_rationale),
                    RC_READ_CALL_LOG, perms);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }
}
