package com.example.calllog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.provider.CallLog;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.List;

public class UploadWorker extends Worker {

    public UploadWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
    }

    @Override
    public Result doWork() {
        // Do the work here--in this case, upload the images.
        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, getApplicationContext().getString(R.string.db_name)).build();

        Log.d(this.getClass().getSimpleName(), "Started worker!");
        @SuppressLint("MissingPermission") Cursor cursor = getApplicationContext().getContentResolver().query(CallLog.Calls.CONTENT_URI,
                null, null,
                null,
                null);

        // Add all new missed calls to database
        while (cursor != null && cursor.moveToNext()) {
            String type = cursor.getString(cursor.getColumnIndex(CallLog.Calls.TYPE));
            if (Integer.parseInt(type) == CallLog.Calls.MISSED_TYPE) {
                String number = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
                Log.d(this.getClass().getSimpleName(), "Number: " + number);

                LocalCall existingCall = db.localCallDao().hasCall(number);

                if (existingCall == null) {
                    LocalCall localCall = new LocalCall();
                    localCall.contact = number;
                    localCall.sent = false;
                    db.localCallDao().insertLocalCall(localCall);
                }
            }
        }
        if (cursor != null) {
            cursor.close();
        } else {
            return Result.failure();
        }


        // Send unsent Calls to server
        List<LocalCall> localCallsToSend = db.localCallDao().getNewLocalCalls();
        //TODO: API Request to send new calls to server
        //TODO: Update in Db that we sent this call using db.localCallDao().updateLocalCall()

        // Indicate whether the task finished successfully with the Result
        return Result.success();
    }
}

