package com.example.calllog;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class LocalCall {
    @PrimaryKey()
    @ColumnInfo(name = "contact")
    public String contact;

    @ColumnInfo(name = "sent")
    public Boolean sent;
}

