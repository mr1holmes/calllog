package com.example.calllog;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface LocalCallDao {

    @Query("SELECT * FROM LocalCall WHERE sent = 0")
    List<LocalCall> getNewLocalCalls();

    @Update
    void updateLocalCall(LocalCall localCall);

    @Insert
    void insertLocalCall(LocalCall localCall);

    @Query("SELECT * FROM LocalCall WHERE contact = :contact")
    LocalCall hasCall(String contact);
}

